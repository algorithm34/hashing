import java.util.ArrayList;
import java.util.Objects;

// โหนดของHash
class HashNode<K, V> {
	K key;
    V value;
      final int hashCode;
      
   // โหนดถัดไป
      HashNode<K, V> next;
   
   // สร้างคอนสตรัคเตอร์
      public HashNode(K key, V value, int hashCode){
    	  this.key = key;
          this.value = value;
          this.hashCode = hashCode;
      }
}

// คลาสสำหรับแสดงตารางของHash
public class Map <K, V>{
	
	// สร้างตัวแปร tableArray เก็บอารเรย์ 
	private ArrayList<HashNode<K, V> > tableArray;

	// จำนวนการเก็บอารเรย์
	private int numTable;
	
	// ขนาดของอารเรย์
	private int size;
	
	// สร้างคอนสตรัคเตอร์สหรับเก็บจำนวน ขนาด
	public Map()
    {
        tableArray = new ArrayList<>();
        numTable = 10;
        size = 0;
 
        // สร้าง empty chains
        for (int i = 0; i < numTable; i++)
        	tableArray.add(null);
    }
	
	public int size(){ 
		return size; 
	}
	
    public boolean isEmpty() { 
    	return size() == 0; 
    }
  
    private final int hashCode (K key) {
        return Objects.hashCode(key);
    }
    
    // ค้นหาตำแหน่งของKey
    private int getTableIndex(K key){
          int hashCode = hashCode(key);
          int index = hashCode % numTable;
          
          index = index < 0 ? index * -1 : index;
          return index;
     }
    
    // การลบ Key ที่กำหนดมา
    public V remove(K key) {
    	
    	// การหา Key ที่กำหนดมา
    	int bucketIndex = getTableIndex(key);
    	int hashCode = hashCode(key);
    	
    	HashNode<K, V> head = tableArray.get(bucketIndex);
    	
    	// ค้นหา Key ในแถว
    	HashNode<K, V> prev = null;
        while (head != null) {
        	
            // ถ้าเจอ Key
            if (head.key.equals(key) && hashCode == head.hashCode)
                break;
            
            prev = head;
            head = head.next;
        }
        
        // ถ้าไม่เจอ Key ในแถว
        if (head == null)
            return null;
 
        // ลดขนาด
        size--;
 
        // แล้วเอา Key ออก
        if (prev != null)
            prev.next = head.next;
        else
            tableArray.set(bucketIndex, head.next);
    	
    	return head.value;
    }
    
    // คืนค่า Key กลับไป
    public V get(K key)
    {
    	// หาตำแหน่งที่ Key กำหนดมา
        int tableIndex = getTableIndex(key);
        int hashCode = hashCode(key);
       
        HashNode<K, V> head = tableArray.get(tableIndex);
        
        // ค้นหา Key
        while (head != null) {
            if (head.key.equals(key) && head.hashCode == hashCode)
                return head.value;
            head = head.next;
        }
 
        // ถ้าไม่เจอ Key
        return null;
    }
    
    // การเพิ่ม Key กับ Value
    public void add(K key, V value)
    {
        // หาตำแหน่งที่ Key กำหนดมา
        int tableIndex = getTableIndex(key);
        int hashCode = hashCode(key);
        HashNode<K, V> head = tableArray.get(tableIndex);
 
        // ตรวจสบว่ามีค่า Key ไหม
        while (head != null) {
            if (head.key.equals(key) && head.hashCode == hashCode) {
                head.value = value;
                return;
            }
            head = head.next;
        }
 
        // หา Key 
        size++;
        head = tableArray.get(tableIndex);
        HashNode<K, V> newNode = new HashNode<K, V>(key, value, hashCode);
        newNode.next = head;
        tableArray.set(tableIndex, newNode);
 
        if ((1.0 * size) / numTable >= 0.7) {
            ArrayList<HashNode<K, V> > temp = tableArray;
            tableArray = new ArrayList<>();
            numTable = 2 * numTable;
            size = 0;
            for (int i = 0; i < numTable; i++)
                tableArray.add(null);
 
            for (HashNode<K, V> headNode : temp) {
                while (headNode != null) {
                    add(headNode.key, headNode.value);
                    headNode = headNode.next;
                }
            }
        }
    }
    
    // ทดสอบโปรแกรม
    public static void main(String[] args)
    {
        Map<String, Integer> map = new Map<>();
        map.add("this", 2);
        map.add("coder", 2);
        map.add("this", 7);
        map.add("hi", 8);
        System.out.println(map.size());
        System.out.println(map.remove("this"));
        System.out.println(map.remove("hi"));
        System.out.println(map.remove("Bye"));
        System.out.println(map.size());
        System.out.println(map.isEmpty());
    }
}

